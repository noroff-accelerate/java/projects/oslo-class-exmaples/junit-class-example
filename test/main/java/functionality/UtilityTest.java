package main.java.functionality;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {

    Utility util = new Utility();

    @Test
    void isEven() {
        assertTrue(util.isEven(2));
        assertFalse(util.isEven(3));
    }

    @Test
    void isPalindrome() {

    }

    @Test
    void convertToLowerCase() {
        String expected = "hello";
        String provided = "HeLLo";
        assertEquals(expected,util.convertToLowerCase(provided));
    }
}