package main.java.functionality;

public class Utility {
    public Boolean isEven(double number){
        return number%2 == 0;
    }

    public Boolean isPalindrome(String text){
        throw new UnsupportedOperationException();
    }

    public String convertToLowerCase(String text){
        return text.toLowerCase();
    }
}
